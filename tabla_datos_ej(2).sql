﻿DROP DATABASE IF EXISTS ej_programacion2;
CREATE DATABASE ej_programacion2;
USE ej_programacion2;

-- Hoja de ejemplos (2)

-- Creación de tabla
CREATE TABLE datos (
  datos_id int (11) NOT NULL AUTO_INCREMENT,
  numero1 int (11) NOT NULL,
  numero2 int (11) NOT NULL,
  suma int (255) DEFAULT NULL,
  resta int (255) DEFAULT NULL,
  rango varchar(5) DEFAULT NULL,
  texto1 varchar(25) DEFAULT NULL,
  texto2 varchar(25) DEFAULT NULL,
  junto varchar(255) DEFAULT NULL,
  longitud varchar(5) DEFAULT NULL,
  tipo  int(11) NOT NULL,
  numero int(5) NOT NULL,
  PRIMARY KEY (datos_id)
)
ENGINE = INNODB,
  CHARACTER SET latin1,
  COLLATE latin1_swedish_ci,
  COMMENT='Esta tabla está para poder actualizarla con los procedimientos',
  ROW_FORMAT=COMPACT,
  TRANSACTIONAL=0;
ALTER TABLE datos
  ADD INDEX numero2_index(numero2);
ALTER TABLE datos
  ADD INDEX nunero_index(numero);
ALTER TABLE datos
  ADD INDEX tipo_index(tipo);