﻿DROP DATABASE IF EXISTS ej_programacion2;
CREATE DATABASE ej_programacion2;
USE ej_programacion2;

-- Hoja de ejemplos (2)

/**
Ejercicio-1.
Procedimiento almacenado que reciba un texto y un carácter. 
Debe indicarte si ese carácter está en el texto. Utilizar LOCATE
**/
DELIMITER //
DROP PROCEDURE IF EXISTS ej1a;
CREATE PROCEDURE ej1a(IN texto varchar(200), IN caracter char(1))
  
  BEGIN
    DECLARE resultado varchar(30) DEFAULT 'El caracter no esta';
 
      IF (LOCATE (caracter,texto)<>0) THEN
    
        SET resultado='El caracter esta';   

      END IF; 
    
      SELECT resultado;
  END //
DELIMITER;

CALL ej1a('estadio','o');

/**
Ejercicio-1b.

Procedimiento almacenado que reciba un texto y un carácter. 
Debe indicarte si ese carácter está en el texto. Utilizar POSITION
**/

 
DELIMITER //
DROP PROCEDURE IF EXISTS ej1b;
CREATE PROCEDURE ej1b(IN texto varchar(200), IN caracter char(1))
  
  BEGIN
    DECLARE resultado varchar(30) DEFAULT 'El caracter no esta';
 
      IF (POSITION(caracter IN texto)<>0) THEN    
        SET resultado='El caracter esta';   
      END IF; 
      SELECT resultado;
  END //
DELIMITER;

  CALL ej1b('sardinero','o');

DELIMITER //
DROP PROCEDURE IF EXISTS ej1b;
CREATE PROCEDURE ej1b(IN texto varchar(200), IN caracter char(1))
  
  BEGIN
    DECLARE resultado varchar(30) DEFAULT 'El caracter no esta';
 
      IF (LOCATE (caracter,texto)<>0) THEN
    
        SET resultado='El caracter esta';   

      END IF; 
    
      SELECT resultado;
  END //
DELIMITER;

CALL ej1b('estadio','o');




/**
Ejemplo 2.
 Realizar un procedimiento almacenado que reciba un texto y un carácter. 
 Te debe indicar todo el texto que haya antes de la primera vez 
 que aparece ese carácter. 
**/
DELIMITER //
DROP PROCEDURE IF EXISTS ej2;
CREATE PROCEDURE ej2(IN texto varchar(200), IN caracter char(1))
  
  BEGIN
    DECLARE resultado varchar(30) DEFAULT NULL;
 
      SET resultado=SUBSTRING_INDEX(texto,caracter,1);   
  
      SELECT resultado;
  END //
DELIMITER ;

CALL ej2('El Racing ascenderá a 1ª este año','o');


/**
Ejemplo 3.
--------------------------------------------------------------
Realizar un procedimiento almacenado que reciba tres números 
y dos argumentos de tipo salida donde devuelva 
el número más grande y el número más pequeño 
de los tres números pasados 
**/

DELIMITER //
DROP PROCEDURE IF EXISTS ej3;
CREATE OR REPLACE PROCEDURE ej3(n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
  BEGIN 
    SET mayor=GREATEST(n1,n2,n3);
    SET menor=LEAST(n1,n2,n3);
  END //
DELIMITER ;

CALL ej3(25,50,75,@v1,@v2);
SELECT @v1,@v2;

/**
Ejemplo 3a

Usando la condición IF
**/

DELIMITER //
DROP PROCEDURE IF EXISTS ej3a;
CREATE OR REPLACE PROCEDURE ej3a(n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
  BEGIN 
    SET mayor=IF(n1>n2,IF(n1>n3,n1,n3), IF (n2>n3,n2,n3));               
    SET menor=IF(n1<n2,IF(n1<n3,n1,n3), IF (n2<n3,n2,n3));
  END //
DELIMITER ;

CALL ej3a(25,50,75,@v1,@v2);
SELECT @v1,@v2;

/**
Ejemplo 4.
Realizar un procedimiento almacenado que muestre cuantos numeros1 y numeros2 son mayores que 50
**/
DELIMITER //
DROP PROCEDURE IF EXISTS ej4;
CREATE OR REPLACE PROCEDURE ej4()
  BEGIN 
    
    DECLARE n1 int;
    DECLARE n2 int;
    DECLARE resultado int;
  
    SELECT COUNT(*) INTO n1 FROM datos d WHERE numero1>50;  
    SET n2=(SELECT COUNT(*) FROM datos d WHERE numero2>50);
    SET resultado=n1+n2;
  
    SELECT resultado;  
  
  END //
DELIMITER ;

CALL ej4();
/**
Ejemplo 5.
Realizar un procedimiento almacenado que calcule la suma y la resta de número1 y número2.
**/
DELIMITER //
DROP PROCEDURE IF EXISTS ej5;
CREATE OR REPLACE PROCEDURE ej5 (n1 int, n2 int, OUT suma int, OUT resta int)
  BEGIN 
       
      SET suma= n1+n2;
    
      SET resta= n1-n2;
    
  END //
DELIMITER ;

CALL ej5(25,50,@v1, @v2);
select @v1,@v2;


/**
Ejemplo 5.
UPDATE 
**/  

DELIMITER //
DROP PROCEDURE IF EXISTS ej5a;
CREATE OR REPLACE PROCEDURE ej5a()
  BEGIN 
    
    UPDATE datos d  
      SET d.suma=d.numero1+d.numero2;
    UPDATE datos d
      SET d.resta=d.numero1-d.numero2;
  
  END //
DELIMITER ;

CALL ej5a();


/**
Realizar un procedimiento almacenado que primero ponga 
todos los valores de suma y resta a NULL y despues 
calcule la suma solamente si el numero1 es mayor que el numero2 
y calcule la resta de numero2-numero1 si el numero2 
es mayor o numero1numero2 si es mayor el numero1
**/
DELIMITER //
  DROP PROCEDURE IF EXISTS ej6;
  CREATE OR REPLACE PROCEDURE ej6()
    BEGIN
            
      UPDATE datos SET suma = NULL;
      UPDATE datos SET resta = NULL;
     
      UPDATE datos SET suma = numero1 + numero2 
        WHERE numero1 > numero2;
      
      UPDATE datos SET resta = numero2 - numero1  
        WHERE numero2 > numero1;

      UPDATE datos SET resta = numero1 - numero2  
        WHERE numero1 >= numero2;

    END //
  DELIMITER ;

  CALL ej6();


/**
Ejemplo 7.
Realizar un procedimiento almacenado que coloque en el campo junto el texto1, texto2 
**/
DELIMITER //
  DROP PROCEDURE IF EXISTS ej7;
  CREATE OR REPLACE PROCEDURE ej7()
    BEGIN
      
      UPDATE datos SET junto = CONCAT(texto1,", ",texto2);

    END //
  DELIMITER ;

  CALL ej7();

  SELECT * FROM datos d;

/**
Ejemplo 8.
Realizar un procedimiento almacenado que coloque en el campo junto el valor NULL. 
Después debe colocar en el campo junto el texto1-texto2 
si el rango es A y si es rango B debe colocar texto1+texto. 
Si el rango es distinto debe colocar texto1 nada más. 
**/
DELIMITER //
  DROP PROCEDURE IF EXISTS ej8;
  CREATE OR REPLACE PROCEDURE ej8()
    BEGIN
        
      UPDATE datos SET junto = NULL;

      UPDATE datos SET junto = 
        IF (rango = 'A', CONCAT(texto1,'-',texto2), 
          IF(rango = 'B', CONCAT(texto1,'+',texto2),
            texto1
          )
        );

    END //
  DELIMITER ;

  
  CALL ej8();
  SELECT * FROM datos d; 

